/*
 * for.c displays a set of numbers while keeping them inline with one another
 */

#include <stdio.h>
#include <math.h>

int intSize(int n){
	/*Pre: subroutine takes in an integer greater than 0*/
	return n > 0 ? log10(n) + 1 : 1; /*Computers truncate floating point when converting to an integer.*/
	/*Post: subroutine returns the number of digits in a number*/
}

int main(void){
	printf("Number: ");
	int s;
	scanf("%d", &s);
	printf("Block: ");
	int b;
	scanf("%d", &b);

	int v = intSize(s);
	for(int i = 0; i < s; i++){
		if(i != 0 && i % b == 0){
			printf("\n");
		}
		printf("%d", i);
		for(int c = intSize(i); c <= v; c++){
			printf("%c", ' ');
		}
	}
	printf("\n");
	return 0;
}
